#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from OnePlus9Pro device
$(call inherit-product, device/oneplus/OnePlus9Pro/device.mk)

PRODUCT_DEVICE := OnePlus9Pro
PRODUCT_NAME := lineage_OnePlus9Pro
PRODUCT_BRAND := OnePlus
PRODUCT_MODEL := LE2120
PRODUCT_MANUFACTURER := oneplus

PRODUCT_GMS_CLIENTID_BASE := android-oneplus-rvo3

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="OnePlus9Pro-user 11 RKQ1.201105.002 2111112053 release-keys"

BUILD_FINGERPRINT := OnePlus/OnePlus9Pro/OnePlus9Pro:11/RKQ1.201105.002/2111112053:user/release-keys
