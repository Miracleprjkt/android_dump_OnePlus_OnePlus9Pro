#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus9Pro.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus9Pro-user \
    lineage_OnePlus9Pro-userdebug \
    lineage_OnePlus9Pro-eng
